<?php

namespace App\Console\Commands;

use App\Service;
use Illuminate\Console\Command;
use Illuminate\Routing\Route;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate sitemap.xml';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sitemap = Sitemap::create()
            ->add(\URL::to('/'));

        Service::all()->each(function (Service $service) use ($sitemap) {
            $sitemap->add(\Url::to("/{$service->slug}"));
        });
        $sitemap->writeToFile(public_path() . "/sitemap.xml");
    }
}
