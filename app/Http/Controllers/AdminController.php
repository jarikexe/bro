<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('admin/main', ['services' => Service::all()]);
    }
}
