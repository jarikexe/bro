<?php

namespace App\Http\Controllers;

use App\Images;
use App\Service;
use Illuminate\Http\Request;

use Intervention\Image\Exception\NotReadableException;
use Validator,Redirect,Response,File;
Use Image;
use Illuminate\Support\Facades\Storage;




class ImagesController extends Controller
{
    public function index(Service $service){
        return Images::where('service_id', $service->id)
            ->orderBy('order', 'asc')
            ->get();
    }

    public function save(Request $request, Images $images)
    {
        request()->validate([
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id' => 'required'
        ]);


        if ($files = $request->file('file')) {
            $image = date('Y-m-d-H:i:s') . "." . $request->file->extension();
            $files->move(public_path('images'), $image);
        }



        $img = Image::make(public_path('images') .'/'. $image);
        $img->resize(320, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save(public_path('images') . "/small-" .$image);


        $img = Image::make(public_path('images') .'/'. $image);
        $img->fit(320, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save(public_path('images') . "/cut-" .$image);

        $images->create([
            'file_name' => $image,
            'service_id' => $request->id,
            'order' => 0
        ]);

        return response('Great! Image has been successfully uploaded.')->status(200);
    }

    public function delete($name)
    {
        $image_record = Images::where('file_name', $name);
        $image = $image_record->first();
        $file_name = $image->file_name;


        unlink(public_path('images') . "/" . $file_name);
        unlink(public_path('images') . "/cut-" . $file_name);
        unlink(public_path('images') . "/small-" . $file_name);

        $image_record->delete();

        return redirect('/services/' . $image->service_id . "/edit");
    }

    public function updateOrder(Request $request)
    {
        $order = $request->order;
        foreach ($order as $counter => $item){
            $counter;
            $image = Images::where('id', $item);
            $image->update(['order' => $counter]);
        }
    }
}
