<?php

namespace App\Http\Controllers;

use App\Order;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Mockery\Exception;
use PhpParser\Error;

final class OrderController extends Controller
{
    private $step = 15;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($page = null)
    {
        $orders = DB::table('orders')
            ->orderBy('id', 'desc')
            ->paginate($this->step);

        return view('admin/order/index', ['orders' => $orders]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Order $order)
    {

        $order_request = json_decode($request->all()['data'], 1);

        $request_real = new Request($order_request);


        $request_real->validate([
            'contact' => 'required|string',
        ]);


        $response = (new \ReCaptcha\ReCaptcha('6LcHocoUAAAAAPkD3etleIqpBYCWiiy7haHkg1w9'))
            ->setExpectedAction('homepage')
            ->setScoreThreshold(0.4)
            ->verify($request->token, $request->ip());

        if (!$response->isSuccess()) {
            return response('Recapcha', 400);
        }


        $msg = '<h1 style="text-align: center; color: green;">Заказ</h1>';
        if(isset($order_request['name'])){
            $order->name = $order_request['name'];
            $msg .= "<div> Имя ". $order->name . "</div>";
        }
        if(isset($order_request['contact'])){
            $order->contact = $order_request['contact'];
            $msg .= "<div> Контакты ".$order->contact ."</div>";
        }

        if(isset($order_request['msg'])){
            $order->msg = $order_request['msg'];
            $msg .= "<div> Сообщение ".$order->msg . "</div>";
        }

        if(isset($order_request['service'])){
            $order->service = $order_request['service'];
            $msg .= "<div>  Заказ на " . $order->service . "</div>";
        }

        $bot_api_key  = '900056334:AAHUXqQru_e1G9YgENOnoQfVUvjO7YdT5Ts';
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://api.telegram.org/bot900056334:AAHUXqQru_e1G9YgENOnoQfVUvjO7YdT5Ts/getUpdates');
        $chats = json_decode($response->getBody(), 1);
        $chats = $chats['result'];
        $chat_id = [];
        foreach ($chats as $chat){
            $chat_id[] = $chat['message']['chat']['id'];
        }
        $chat_id = array_unique($chat_id);

        foreach ($chat_id as $id){
            try{
                $client->request('GET', 'https://api.telegram.org/bot900056334:AAHUXqQru_e1G9YgENOnoQfVUvjO7YdT5Ts/sendMessage?chat_id='
                    .$id.
                    '&text='
                    . strip_tags($msg));
            }catch(ClientException $e){
                $response = $e->getResponse();
            }
        }

        mail('the.sun.grad@gmail.com', 'Заказ', $msg);

        $order->save();
        return http_response_code(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
