<?php

namespace App\Http\Controllers;

use App\Settings;
use Illuminate\Http\Request;
use App\Service;
use PhpParser\Error;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Settings::all();
        $set = [];
        foreach ($settings as $setting) {
            $set[$setting['name']] = $setting->value;
        }
        $services = Service::orderBy('id', 'desc')->get();

        return view('pages/home', ['services' => $services, 'settings' => $set]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $service = Service::create([
            'title' => request('title'),
            'description' => request('description'),
            'slug' => request('slug'),
            'seo_description' => request('seo_description')
        ]);


        return redirect("/services/$service->id/edit");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $settings = Settings::all();
        $set = [];
        foreach ($settings as $setting) {
            $set[$setting['name']] = $setting->value;
        }

        $services = Service::all();
        $service = Service::where('slug', $slug)
            ->first();

        return view('pages/service', ['service' => $service, 'services' => $services, 'settings' => $set]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('admin/service.edit', ['service' => $service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $service->title = request('title');
        $service->description = request('description');
        $service->slug = request('slug');
        $service->seo_description = request('seo_description');

        $service->save();

        return redirect('/admin');

    }

    /**$file_name
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {

        foreach ($service->images as $image){
            try{
                unlink(public_path('images') . "/" .  $image->file_name);
                unlink(public_path('images') . "/cut-" .  $image->file_name);
                unlink(public_path('images') . "/small-" .  $image->file_name);
            } catch (Error $e){

            }

        }

        $service->delete();

        return redirect('/admin');
    }
}
