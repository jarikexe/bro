<?php

namespace App\Http\Controllers;

use App\Settings;
use App\User;
use Illuminate\Http\Request;
use PhpParser\Error;

final class SettingsController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $settings = Settings::all();
        $set = [];
        foreach ($settings as $setting) {
            $set[$setting['name']] = $setting->value;
        }
        return view('admin/settings.edit', ['settings' => $set]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $settings = $request->all();
        foreach ($settings as $name => $_setting) {
            if (substr($name, 0, 1) == '_') {
                continue;
            }
            $setting = Settings::where('name', $name);
            $setting->update(['value' => $_setting]);
        }
        return redirect('/settings/edit');
    }

    public function slederUpdate(Request $request)
    {
        request()->validate([
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($files = $request->file('file')) {
            $image = date('Y-m-d-H:i:s') . "." . $request->file->extension();
            $files->move(public_path('images'), $image);
            $slider = Settings::where('name', 'slider');
            if(count($slider->get())){
                $slides = \unserialize($slider->first()->value);
            }else{
                $slides = [];
            }
            $slides[] = $image;

            $slider->update(['value' => \serialize($slides)]);
        }

        return redirect('/settings/slider/edit');
    }
    public function slideRemoce($name)
    {
        $slides = unserialize(Settings::where('name', 'slider')->first()->value);
        if (($key = array_search($name, $slides)) !== false) {
            unset($slides[$key]);
        }
        try{
            unlink(public_path('images') . "/" . $name);
        }catch (Error $error){

        }

        Settings::where('name', 'slider')->update(['value' => \serialize($slides)]);
        return redirect('/settings/slider/edit');
    }
    public function sliderShow()
    {
        $settings = Settings::where('name', 'slider');
        if(count($settings->get())){
            $slider = \unserialize($settings->first()->value);
            return view('admin/settings.slider_edit', ['images' => $slider]);

        }else{
            return view('admin/settings.slider_edit', ['images' => []]);
        }

    }
    public function slederOrder(Request $request)
    {
        $order = $request->order;

        Settings::where('name', 'slider')->update(['value' => \serialize($order)]);
    }
    public function passwordChange()
    {
        return view('admin/settings.password_change');
    }
    public function passwordStore(Request $request)
    {
        $rules = [
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'new_password1' => 'required'
        ];
        $customMessages = [
            'old_password.required' => 'Введите старый пароль.',
            'new_password.required' => 'Введите новый пароль',
            'new_password.min' => 'Новый пароль должен быть минимум 8 символов',
            'new_password1.required' => 'Повторите новый пароль',

        ];

        $this->validate($request, $rules, $customMessages);

        if($request->new_password != $request->new_password1){
            return \Redirect::back()->withErrors(['Пароли не совпадают']);
        }

        $user = User::where('name', 'admin');

        if (!\Hash::check($request->old_password, $user->first()->password)) {
            return \Redirect::back()->withErrors(['Старый пароль неверный']);
        }

        $user->update(['password' => \Hash::make($request->new_password)]);

        return redirect('/admin');

    }

}
