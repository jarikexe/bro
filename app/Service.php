<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

final class Service extends Model
{
    protected $fillable = ['title', 'description', 'seo_description', 'slug'];

    public function images(){
        return $this->hasMany(Images::class,"service_id","id")->orderBy('order');
    }
}
