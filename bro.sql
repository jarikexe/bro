-- MySQL dump 10.13  Distrib 8.0.18, for Linux (x86_64)
--
-- Host: localhost    Database: bro
-- ------------------------------------------------------
-- Server version	8.0.18-0ubuntu0.19.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` bigint(20) unsigned NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `images_service_id_foreign` (`service_id`),
  CONSTRAINT `images_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,2,'2020-01-05-22:19:04.jpeg',1,'2020-01-05 20:19:04','2020-01-05 20:21:00'),(2,2,'2020-01-05-22:19:33.jpeg',2,'2020-01-05 20:19:33','2020-01-05 20:21:00'),(3,2,'2020-01-05-22:20:55.jpeg',0,'2020-01-05 20:20:55','2020-01-05 20:21:00'),(4,3,'2020-01-05-22:23:51.jpeg',0,'2020-01-05 20:23:52','2020-01-05 20:23:52'),(5,3,'2020-01-05-22:24:27.jpeg',0,'2020-01-05 20:24:27','2020-01-05 20:24:27');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (14,'2014_10_12_000000_create_users_table',1),(15,'2014_10_12_100000_create_password_resets_table',1),(16,'2019_08_19_000000_create_failed_jobs_table',1),(17,'2019_12_20_033408_create_services_table',1),(18,'2019_12_23_222009_add_slug_and_seo_description',1),(19,'2019_12_25_173159_create_images_table',1),(20,'2019_12_26_220307_create_settings_table',1),(21,'2019_12_27_221458_create_orders_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `msg` longtext COLLATE utf8mb4_unicode_ci,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'test','+80680988599','test',NULL,'2020-01-05 20:16:27','2020-01-05 20:16:27');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (2,'Сварочные работы','<p>SunGraD&nbsp;производит сварочные работы, имея в наличии собственные цеха в Киеве, оснащенные прогрессивным оборудованием. Наш персонал &mdash; креативные мастера, которые готовы браться за самые сложные задачи и выполнять их с блеском.&nbsp;<br />\r\nПрофессиональная бригада рабочих работает с 8:00 до 21:00, быстро выезжает на объект без опозданий. Специалисты готовы выполнить срочный ремонт, произвести монтаж сварочного изделия в любых, даже самых сложных условиях. Есть возможность нанять мастера для работы на постоянной основе.</p>\r\n\r\n<p>Мы создаем качественные и долговечные изделия из металла. Это могут быть маленькие декоративные элементы для украшения забора, частной территории, а также каркасы жилых домов, коммерческих зданий. Мы изготавливаем металлоконструкции разных размеров и предлагаем следующие виды работ:<br />\r\n&nbsp; &nbsp; &bull; сантехническая сварка;<br />\r\n&nbsp; &nbsp; &bull; вальцевание труб;<br />\r\n&nbsp; &nbsp; &bull; ремонт авто запчастей;<br />\r\n&nbsp; &nbsp; &bull; создание каркасов, козырьков, беседок, лестниц, навесов;<br />\r\n&nbsp; &nbsp; &bull; установка металлических ограждений;<br />\r\n&nbsp; &nbsp; &bull; выполнение нестандартных задач и прочее.<br />\r\n&nbsp;<br />\r\nМастера имеют большой опыт работы с&nbsp;аргонной&nbsp;сваркой и полуавтоматом. Это открывает фактически безграничные возможности для работы со всеми существующими видами металла. Аргон позволяет создавать конструкции идеальной формы с минимальными деформациями во время сварки.</p>','2020-01-05 20:18:35','2020-01-05 20:18:35','svarochnye-raboty-kiev',NULL),(3,'СТО','<p>В СТО происходит обслуживание автомобилей, ремонт и замена запчастей. Мы выполняем все виды услуг:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>качественно;</p>\r\n	</li>\r\n	<li>\r\n	<p>быстро;</p>\r\n	</li>\r\n	<li>\r\n	<p>недорого.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Мастерская оборудована современной техникой. Все работы производят квалифицированные специалисты, которые найдут выход из любой сложной ситуации.</p>\r\n\r\n<p>Если нужно, мы сами приедем к вам домой, в автопарк, на стоянку и сделаем ремонт за короткое время. Наличие собственного сварочного цеха дает возможность быстро ремонтировать сложные поломки, восстанавливать работоспособность деталей. В этом &mdash; ваша экономия времени и финансов.</p>\r\n\r\n<p>Если у вас есть свой бизнес, или вам нужно по другим причинам часто обращаться к услугам СТО, откройте раздел Партнерство и Сотрудничество, внимательно ознакомьтесь с информацией. У вас есть отличная возможность каждый месяц экономить 50% на ремонте автотранспорта, различных услугах СТО, а также на заказе новых деталей.</p>\r\n\r\n<p>2 страница</p>\r\n\r\n<p>СТО корпорации&nbsp;SunGrad&nbsp;выстроена по европейскому стандарту. При создании специалисты брали за основу существующие в Европе&nbsp;БПО. Благодаря этому станция техобслуживания готова предоставлять услуги различной сложности.</p>\r\n\r\n<p>Квалифицированные мастера сделают все возможное, чтобы вы получили качественно выполненную работу в срок и при этом существенно сэкономили. Наш персонал состоит исключительно из профессионалов, способных произвести ремонт всех деталей, а также проконсультировать владельца авто по поводу дальнейшей эксплуатации той или иной запчасти. Специалисты, имея богатый опыт и постоянно обучаясь, не боятся сложных и редких поломок, смело берутся за все заказы.</p>\r\n\r\n<p>Какие услуги мы оказываем:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>ремонт и обслуживание двигателя;</p>\r\n	</li>\r\n	<li>\r\n	<p>регулировка развал-схождения;</p>\r\n	</li>\r\n	<li>\r\n	<p>шиномонтаж;</p>\r\n	</li>\r\n	<li>\r\n	<p>обслуживание коробки передач, детальный ремонт любых сложных поломок;</p>\r\n	</li>\r\n	<li>\r\n	<p>заправка и ремонт кондиционера;</p>\r\n	</li>\r\n	<li>\r\n	<p>реставрация кузова;</p>\r\n	</li>\r\n	<li>\r\n	<p>плановое ТО;</p>\r\n	</li>\r\n	<li>\r\n	<p>ремонт электронных запчастей и прочее.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Помимо всего вышесказанного мы постоянно работаем на улучшение взаимоотношений с клиентом, делаем сотрудничество взаимовыгодным и приятным. Для постоянных клиентов у нас есть накопительные скидки, о которых вы сможете прочесть детально в разделе Партнерство и Сотрудничество.</p>','2020-01-05 20:23:30','2020-01-05 20:23:30','sto-kiev-remont-avtomobiley',NULL);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'title','Сварочные работы / Строительные работы / Ремонт авто',NULL,'2020-01-05 19:59:04'),(2,'about_us','Мы рады приветствовать вас на сайте SunGrad. Мы представляем собой сотрудничество отечественных и иностранных бизнесменов в Украине. Это прекрасная возможность предоставить гражданам сервис европейского и американского уровня.\r\n \r\nЧто мы предлагаем:\r\n    • собственное производство, а значит — низкие цены без переплат;\r\n    • оперативное выполнение работы — мы уважаем каждого клиента и ценим его время;\r\n    • высокое качество работы — об этом свидетельствует наличие постоянных клиентов и положительных отзывов;\r\n    • стремление к сотрудничеству — мы готовы оказать услуги по себестоимости или вознаградить финансово всех, кто помогает нам развиваться.',NULL,'2020-01-05 19:59:04'),(3,'email','the.sun.grad@gmail.com',NULL,'2020-01-05 19:59:04'),(4,'tel1','+111111111111111',NULL,'2020-01-05 19:59:04'),(5,'tel2','+111111111111111',NULL,'2020-01-05 19:59:04'),(6,'facebook','https://www.facebook.com/',NULL,'2020-01-05 19:59:04'),(7,'instagram','https://www.instagram.com/',NULL,'2020-01-05 19:59:04'),(8,'telegram','https://web.telegram.org/',NULL,'2020-01-05 19:59:04'),(9,'slider','a:3:{i:0;s:24:\"2020-01-05-22:25:54.jpeg\";i:1;s:24:\"2020-01-05-22:25:47.jpeg\";i:2;s:24:\"2020-01-05-22:15:28.jpeg\";}',NULL,'2020-01-05 20:26:01');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin',NULL,'$2y$10$hioULcttUBs9f4JPUfU37u7cwcJPsyYjs.L.6okZbilUrOuziyvE6',NULL,NULL,'2020-01-05 19:49:32');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-06  0:28:31
