require('./bootstrap');
require('normalize.css');
const axios = require('axios');
import Glide  from '@glidejs/glide'
import MicroModal from 'micromodal';
import Viewer from 'viewerjs';
import 'viewerjs/dist/viewer.css';

const mapHtml = "<iframe\n" +
    "id='mapEmb' src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d325518.68780316407!2d30.252511957059642!3d50.4016990487754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cf4ee15a4505%3A0x764931d2170146fe!2z0JrQuNC10LIsIDAyMDAw!5e0!3m2!1sru!2sua!4v1576699688253!5m2!1sru!2sua\"\n" +
    "width=\"600\" height=\"470\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>\n";



if(document.querySelector('#image') !== null) {
    const viewer = new Viewer(document.getElementById('image'), {
        modal: true,
        toolbar: false,
        title: false,
        movable: false,
        url(image) {
            return image.src.replace('small-', '');
        },
    });
}

if(document.querySelector('#map') !== null){
   const map = document.querySelector('#map');
    window.addEventListener('scroll', function (event) {

        if (isInViewport(map) && document.querySelector('#mapEmb') == null) {
            map.innerHTML = mapHtml;
        }
    }, false);
}

let isInViewport = function (elem) {
    var distance = elem.getBoundingClientRect();
    return (
        distance.top >= 0 &&
        distance.left >= 0 &&
        distance.bottom <= (window.innerHeight +600 || document.documentElement.clientHeight) &&
        distance.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};

document.querySelector("#close-menu").onclick = function()
{
    try {
        document.querySelector('#head').classList.remove('open');
    }catch (e) {
        console.info('no such class');
    }
    document.querySelector('#head').classList.add('close');
}
document.querySelector("#open-menu").onclick = function()
{
    try {
        document.querySelector('#head').classList.remove('close');
    }catch (e) {
        console.info('no such class');
    }
    document.querySelector('#head').classList.add('open');
}



//modals
MicroModal.init({
    openTrigger: 'data-custom-open', // [3]
    disableScroll: true, // [5]
    disableFocus: false, // [6]
    awaitOpenAnimation: false, // [7]
    awaitCloseAnimation: false, // [8]
    debugMode: true // [9]
});

if(document.querySelector('.glide') !== null){
    new Glide('.glide', {
        slidesToShow: 1,
        focusAt: 'center',
        autoplay: 4000,
        draggable: true
    }).mount({  });
}

if(document.querySelector('.glide2') !== null) {
    new Glide('.glide2', {
        perView: 1,
        controls: true,
        type: 'carousel',
        focusAt: 'center',
    }).mount({  });
}

const orderBtn = document.querySelectorAll('.__order');

orderBtn.forEach(function (userItem) {
    userItem.onclick = function (e) {
        const name = userItem.parentNode.parentNode.parentNode.parentNode.querySelector("h3 a")
            ?userItem.parentNode.parentNode.parentNode.parentNode.querySelector("h3 a").innerText
            :userItem.parentNode.parentNode.querySelector("h1").innerText;
        document.querySelector('#__orderName').value = name;
        if(document.querySelector('#__orderName2')) {
            document.querySelector('#__orderName2').value = name;
        }
    }
});

const forms = document.querySelectorAll('form');
let errors = document.querySelectorAll('.errors');
let modals = document.querySelectorAll('.modal');


forms.forEach(function (userItem) {
    // if()
    // const id = userItem.getElementsByClassName("anchors")[0].id;
    // if(id != 'login-form'){
        userItem.onsubmit = function (e) {
            e.preventDefault();
            let sbmtButton = userItem.querySelector("button");

            sbmtButton.disabled = true;

            grecaptcha.ready(function() {
                grecaptcha.execute('6LcHocoUAAAAAITkd3OuSwY6JiZqqbQjEKxkNCek', {action: 'homepage'}).then(function(token) {
                    errors.forEach(function (userItem) {
                        userItem.innerHTML = "";
                    });

                    const data = toJSONString(userItem);
                    axios.post('/orders', {
                        data,
                        token
                    })
                        .then(function (response) {
                            sbmtButton.disabled = false;
                            modals.forEach(function(item) {
                                if(item.classList.contains('is-open')){
                                    item.classList.remove('is-open');
                                }


                            });
                            MicroModal.close();
                            MicroModal.show('modal-success');
                        })
                        .catch(function (error) {
                            userItem.querySelector('.errors').innerHTML = "<p>Поле контактов обязательно должно быть заполнено, если вы заполнили поле, но ошибка не исчезает, воспользуйтесь телефоном или электронной почтой для связи с нами</p>";
                        });
                });
            });
        }
    // }
});


let toJSONString = function( form ) {
    var obj = {};
    var elements = form.querySelectorAll( "input, select, textarea" );
    for( var i = 0; i < elements.length; ++i ) {
        var element = elements[i];
        var name = element.name;
        var value = element.value;

        if( name ) {
            obj[ name ] = value;
        }
    }

    return JSON.stringify( obj );
};


