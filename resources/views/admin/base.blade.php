<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/ico" sizes="16x16" href="favicon.ico">
    <title>Админ зона | Sun Grad</title>
    <!-- Bootstrap Core CSS -->
    <link href="/admin-assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="/admin-assets/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="/admin-assets/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/admin-assets/css/style.css" rel="stylesheet">
    <link href="/admin-assets/css/dropzone.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <![endif]-->
    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
</head>

<body>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
        <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
            <div class="top-left-part"><a class="logo" href="/"><span class="hidden-xs">SunGrud.net</span></a></div>
            <ul class="nav navbar-top-links navbar-right hidden-xs">
                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
        </div>
    </nav>
    <div class="navbar-default sidebar nicescroll" role="navigation">
        <div class="sidebar-nav navbar-collapse ">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="/admin" class="waves-effect"><i class=" ti-home fa-fw"></i>Услуги</a>
                </li>
                <li>
                    <a href="/orders" class="waves-effect"><i class="  ti-check-box fa-fw"></i>Заказы</a>
                </li>
                <li>
                    <a href="/settings/edit" class="waves-effect"><i class=" ti-settings fa-fw"></i>Настройки</a>
                </li>
                <li>
                    <a href="/settings/slider/edit" class="waves-effect"><i class="ti-layout-slider-alt fa-fw"></i>Слайдер</a>
                </li>
                <li>
                    <a href="/settings/change-password" class="waves-effect"><i class="ti-lock fa-fw"></i>Сменить пароль</a>
                </li>
            </ul>
        </div>
    </div>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-12">
                    <h4 class="page-title">Админ зона</h4>
                </div>
            </div>
            @yield('content')
        </div>
    </div>
    <footer class="footer text-center"> 2019 &copy;</footer>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.10.1/Sortable.min.js" integrity="sha256-9D6DlNlpDfh0C8buQ6NXxrOdLo/wqFUwEB1s70obwfE=" crossorigin="anonymous"></script>

<script src="/admin-assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/admin-assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/admin-assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="/admin-assets/js/jquery.nicescroll.js"></script>
<script src="/admin-assets/js/waves.js"></script>
<script src="/admin-assets/js/myadmin.js"></script>
<script src="/admin-assets/js/dashboard1.js"></script>
<script src="/js/admin/admin.js"></script>
</body>

</html>
