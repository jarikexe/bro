

@extends('admin/base')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-xs-6">
                        <h3>Услуги</h3>
                    </div>
                    <div class="col-xs-6 text-right">
                        <a href="/service/create" class="btn btn-success">Добавить</a>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Заголовок</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($services as $service)
                            <tr>
                                <td>{{$service->id}}</td>
                                <td>{{$service->title}}</td>
                                <td class="text-right">
                                    <a class="btn btn-primary" href="/services/{{$service->id}}/edit">Редактировать</a>
                                    <a class="btn btn-danger" href="/services/{{$service->id}}/delete">Удалить</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
