@extends('admin.base')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-xs-6">
                        <h3>Заказы</h3>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Контакты</th>
                            <th>Имя</th>
                            <th>Услуга</th>
                            <th>Сообщение</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->contact ?: '--//--' }}</td>
                                <td>{{$order->name ?: '--//--'}}</td>
                                <td>{{$order->service ?: '--//--'}}</td>
                                <td>{{$order->msg ?: '--//--'}}</td>
                            </tr>
                        @endforeach
                        </tbody>


                    </table>
                    {{ $orders->links() }}

                </div>
            </div>
        </div>
    </div>
@endsection
