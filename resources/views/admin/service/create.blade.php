@extends('admin.base')
@section('content')

    <div class="white-box">
        <form method="POST" action="/services" class="form-horizontal form-material">
            @csrf
            <div class="form-group">
                <label class="col-md-12">Названия предоставляемого сервиса</label>
                <div class="col-md-12">
                    <input type="text" name="title" placeholder="Сварочные работы" id="title" class="form-control form-control-line"> </div>
            </div>

            <div class="form-group">
                <label class="col-md-12">Slug</label>
                <div class="col-md-12">
                    <input type="text" name="slug" id="slug" placeholder="svarochnie-raboti" class="form-control form-control-line"> </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label for="exampleFormControlTextarea1">SEO Описание (50–160 символов)</label>
                    <textarea class="form-control" name="seo_description" placeholder="Текст который будет отображатся в гугл поиске" rows="2"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-12" for="description">Описание</label>
                <br>
                    <textarea name="description" id='editor' style='margin-top:30px;'></textarea>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-success">Сохранить</button>
                </div>
            </div>
        </form>
    </div>

@endsection
