@extends('admin.base')
@section('content')

    <div class="white-box">
        <form method="POST" action="/services/{{$service->id}}" class="form-horizontal form-material">
            @csrf
            @method("PUT")
            <div class="form-group">
                <label class="col-md-12">Названия предоставляемого сервиса</label>
                <div class="col-md-12">
                    <input value="{{$service->title}}" type="text" name="title" placeholder="Сварочные работы" class="form-control form-control-line"> </div>
            </div>
            <div class="form-group">
                <label class="col-md-12">Slug</label>
                <div class="col-md-12">
                    <input value="{{$service->slug}}" type="text"  readonly="readonly" name="slug" placeholder="svarochnie-raboti" class="form-control form-control-line"> </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="exampleFormControlTextarea1">SEO Описание (50–160 символов)</label>
                    <textarea class="form-control" name="seo_description" placeholder="Текст который будет отображатся в гугл поиске" rows="2">{{$service->seo_description}}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-12" for="description">Описание</label>
                <br>

                    <textarea name="description" id='editor' style='margin-top:30px;'>{{$service->description}}</textarea>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-success">Обновить</button>
                </div>
            </div>
        </form>


        <h2>Картинки</h2>
        <input type="hidden" id="service_id" value="{{$service->id}}">
       <div id="dropzone" class="dropzone">
           <div class="dz-message" data-dz-message>
               <h1><span class=" ti-upload"></span></h1>
               <span>Загрузка картинок</span>
           </div>
       </div>

        <h2>
            Последовательность
        </h2>
        <div id="uploaded" class="drugable row">

        </div>

    </div>

    <!-- Include the Quill library -->
    <script src="https://cdn.quilljs.com/1.0.0/quill.js"></script>
    <script src="/admin-assets/js/dropzone.js"></script>
@endsection
