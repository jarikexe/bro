@extends('admin.base')
@section('content')
    <div class="white-box">
        <form method="POST" action="/settings/update" class="form-horizontal form-material">
            @csrf
            @method("PUT")
            <div class="form-group">
`                <div class="col-md-12">
                    <input type="text" placeholder="" name="title" value="{{$settings['title'] ?? ""}}" class="form-control form-control-line"> </div>
            </div>
            <div class="form-group">
                <label class="col-md-12">Описание</label>
                <div class="col-md-12">
                    <textarea rows="5" name="about_us" class="form-control form-control-line" spellcheck="false" data-gramm="false">{{$settings['about_us'] ?? ""}}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="example-email" class="col-md-12">Email</label>
                <div class="col-md-12">
                    <input type="email" name="email" value="{{$settings['email'] ?? ""}}" placeholder="johnathan@admin.com" class="form-control form-control-line" name="example-email" id="example-email"> </div>
            </div>
            <div class="form-group">
                <label class="col-md-12">Номер телефона 2</label>
                <div class="col-md-12">
                    <input type="tel" name="tel1" value="{{$settings['tel1'] ?? ""}}" placeholder="+8068000000" class="form-control form-control-line"> </div>
            </div>
            <div class="form-group">
                <label class="col-md-12">Номер телефона 2</label>
                <div class="col-md-12">
                    <input type="tel"  name="tel2" value="{{$settings['tel2']?? ""}}" placeholder="+8068000000" class="form-control form-control-line"> </div>
            </div>
            <div class="form-group">
                <label class="col-md-12">Facebook link</label>
                <div class="col-md-12">
                    <input type="text" value="{{$settings['facebook']?? ""}}"  name="facebook" placeholder="" class="form-control form-control-line"> </div>
            </div>
            <div class="form-group">
                <label class="col-md-12">Instagram link</label>
                <div class="col-md-12">
                    <input type="text" name="instagram" value="{{$settings['instagram']?? ""}}"  placeholder="" class="form-control form-control-line"> </div>
            </div>
            <div class="form-group">
                <label class="col-md-12">Telegram link</label>
                <div class="col-md-12">
                    <input type="text" name="telegram" value="{{$settings['telegram']?? ""}}"  placeholder="" class="form-control form-control-line"> </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-success">Обновить настройки</button>
                </div>
            </div>
        </form>
    </div>
@endsection
