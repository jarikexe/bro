@extends('admin.base')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-xs-6">
                        <h3>Смена пароля</h3>
                    </div>
                </div>
                <h4><a href="https://www.lastpass.com/ru/password-generator">Генератор надежных паролей</a></h4>
                <h4><a href="https://haveibeenpwned.com/Passwords">Проверить надежность пароля</a></h4>
                <form action="/settings/change-password" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputPassword1">Старый пароль</label>
                        <input type="password" name="old_password" class="form-control" id="exampleInputPassword1"
                               placeholder="Password">
                    </div>
                    @if($errors->has('old-password'))
                        <div class="alert alert-danger">{{$errors->first('old-password')}}</div>
                    @endif
                    <div class="form-group">
                        <label for="exampleInputPassword1">Новый пароль</label>
                        <input type="password" name="new_password" class="form-control" id="exampleInputPassword1"
                               placeholder="Password">
                    </div>
                    @if($errors->has('new-password'))
                        <div class="alert alert-danger">{{$errors->first('new-password')}}</div>
                    @endif
                    <div class="form-group">
                        <label for="exampleInputPassword1">Повторить новый пароль</label>
                        <input type="password" name="new_password1" class="form-control" id="exampleInputPassword1"
                               placeholder="Password">
                    </div>
                    @if($errors->has('new-password1'))
                        <div class="alert alert-danger">{{$errors->first('new-password1')}}</div>
                    @endif
                    <div class="form-group">
                        <button class="btn btn-success">Сохранить</button>
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

        </div>
    </div>
@endsection
