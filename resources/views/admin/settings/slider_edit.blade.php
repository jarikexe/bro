@extends('admin.base')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <form action="/settings/slider/save" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="input-group">

                        <div class="custom-file row">
                            <div class="col-xs-12">
                                <input type="file" name="file" class="custom-file-input col-xs-6" id="inputGroupFile01"
                                       aria-describedby="inputGroupFileAddon01">
                                <button class="btn btn-success col-xs-4">Загрузить картинку</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="white-box">
                <div id="slides_sort" class="row">
                    @if($images )
                        @foreach($images as $image)
                            <div class="col-xs-4 slide-prev" data-slide="{{$image}}">
                                <div class="image-cover">
                                    <img src="/images/{{$image}}" class="col-xs-12" alt="">
                                </div>
                                <a href="/settings/slider/{{$image}}/remove">Удалить</a>
                            </div>
                        @endforeach
                    @endif
                </div>

            </div>
        </div>
    </div>
@endsection
