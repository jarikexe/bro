<html>
<head>
    <title>@yield('title') | Sun Grad</title>
    <meta name="viewport" content="initial-scale=1">
    <meta name="description" content="@yield('description', ''); | Sun Grad"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/fonts.css">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link rel="icon" type="image/ico" sizes="16x16" href="favicon.ico">
    <link rel="stylesheet" href="node_modules/@glidejs/glide/dist/css/glide.core.min.css">
    <link rel="stylesheet" href="node_modules/@glidejs/glide/dist/css/glide.theme.min.css">
</head>
<body
    class="@section('page-class')
        basic
@endsection
        "
>
<div id="head" class="head close">
    <div class="head-left">
        <div class="logo-wrp">
            <div class="mobile-nav">
                <span id="open-menu" class="icon-menu"></span>
                <span id="close-menu" class="icon-close"></span>
            </div>
            <a class="logo" href="/">
                <img src="/img/logo.svg" alt="Логотип">
            </a>
        </div>
        <nav>
            <ul>

                <li><a href="/">Главная</a></li>
                @foreach ($services ?? '' as $service)
                    <li><a href="/{{$service->slug}}">{{$service->title}}</a></li>
                @endforeach
            </ul>
        </nav>
    </div>
    <div class="phone-numbers">
        <div>
            <a href="tel:{{$settings['tel1'] ?? ''}}">{{$settings['tel1'] ?? ''}}</a>
            <a href="tel:+{{$settings['tel1'] ?? ''}}">{{$settings['tel2'] ?? ''}}</a>
        </div>
    </div>
</div>

<div class="container">
    @yield('content')
    <section class="contacts">
        <span class="h1">Контакты</span>
        <div class="contacts-in">
            <div id="map" class="map">

            </div>
            <div class="form">
                <span class="h1 h1-white">Обратная связь</span>
                <form>
                    <div class="inputs">
                        <input class="contact-form cf-input" name="name" type="text" placeholder="Имя">
                        <input class="contact-form cf-input" name="contact" type="text"
                               placeholder="Телефон/Эмаил">
                        <textarea class="contact-form cf-text" name="msg"
                                  placeholder="Ваше сообщение ..."></textarea>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-success cf-button">Отправить</button>
                    </div>
                    <div class="errors">
                    </div>
                </form>
                <div class="phone-email">
                    <p><a href="tel:{{$settings['tel1'] ?? ''}}">{{$settings['tel1'] ?? ''}}</a></p>
                    <p><a href="tel:+{{$settings['tel1'] ?? ''}}">{{$settings['tel2'] ?? ''}}</a></p>
                    <p>{{$settings['email'] ?? ''}}</p>
                </div>
            </div>

        </div>
    </section>

    <section class="social">
        <span class="h1">Мы в соц сетях</span>
        <div class="social-in">
            <a target="_blank" href="{{$settings['facebook'] ?? ''}}">
                <span class="icon-facebook"></span>
            </a>
            <a target="_blank" href="{{$settings['instagram'] ?? ''}}">
                <span class="icon-instagram1"></span>
            </a>
            <a target="_blank" href="{{$settings['telegram'] ?? ''}}">
                <span class="icon-telegram"></span>
            </a>
        </div>
    </section>
</div>

<footer class="main-footer">
    <nav class="footer-menu">
        <ul>
            <li><a href="/">Главная</a></li>
            @foreach ($services ?? '' as $service)
                <li><a href="/{{$service->slug}}">{{$service->title}}</a></li>
            @endforeach
        </ul>
    </nav>
    <div><a href="/">SunGrud.net</a></div>
    <div>© 2019-{{ now()->year }}</div>
</footer>


<div class="modal modal-1 micromodal-slide" id="modal-1" aria-hidden="true">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close>
        <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
            <div class="modal__header">
                <span class="h1 h1-white">Обратная связь</span>

                <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
            </div>
            <div class="modal__content" id="modal-1-content">
                <div class="form">
                    <form>
                        <div class="inputs">
                            <input class="contact-form cf-input" name="name" type="text" placeholder="Имя">
                            <input class="contact-form cf-input" name="contact" type="text"
                                   placeholder="Телефон/Email">
                            <textarea class="contact-form cf-text" name="msg"
                                      placeholder="Ваше сообщение ..."></textarea>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-success cf-button">Отправить</button>
                        </div>
                        <div class="errors">
                        </div>
                    </form>

                    <div class="phone-email">
                        <p><a href="tel:{{$settings['tel1'] ?? ''}}">{{$settings['tel1'] ?? ''}}</a></p>
                        <p><a href="tel:{{$settings['tel1'] ?? ''}}">{{$settings['tel2'] ?? ''}}</a></p>
                        <p>{{$settings['email'] ?? ''}}</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="modal modal-1 micromodal-slide" id="modal-2" aria-hidden="true">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close>
        <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
            <div class="modal__header">
                <span class="h1 h1-white">Заказать</span>

                <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
            </div>
            <div class="modal__content" id="modal-1-content">
                <div class="form">
                    <form>
                        <div class="inputs">
                            <input class="contact-form cf-input" name="name" type="text" placeholder="Имя">
                            <input class="contact-form cf-input" name="contact" type="text"
                                   placeholder="Телефон/Email">
                            <textarea class="contact-form cf-text" name="msg"
                                      placeholder="Опишите в кратце что вас интересует (это поле не обизательное) ..."></textarea>
                            <span>Заказать: </span><input type="text" id="__orderName" name="service" class="invisible"
                                                          disabled="" name="order" value="Сварочные работы">
                        </div>
                        <div>
                            <button type="submit" class="btn btn-success cf-button">Отправить</button>
                        </div>
                        <div class="errors">
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal modal-1 micromodal-slide" id="modal-3" aria-hidden="true">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close>
        <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
            <div class="modal__header">
                <span class="h1 h1-white">Заказать</span>

                <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
            </div>
            <div class="modal__content" id="modal-1-content">
                <div class="form">
                    <form>
                        <div class="inputs">
                            <input type="tel" class="phone-numbers" placeholder="Номер телефона" required="required"
                                   name="contact" value="+380">
                            <input type="hidden" id="__orderName2" class="invisible" disabled="" name="service"
                                   value="Сварочные работы">
                        </div>
                        <div>
                            <button type="submit" class="btn btn-success cf-button">Отправить</button>
                        </div>
                        <div class="errors">

                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal modal-1 micromodal-slide" id="modal-success" aria-hidden="true">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close>
        <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
            <div class="modal__header">
                <span class="h1 h1-white">Успех !</span>

                <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
            </div>
            <div class="modal__content" id="modal-1-content">
                <span class="icon-checkmark check"></span>
                <p>Мы скоро с вами свяжемся!</p>
            </div>

        </div>
    </div>
</div>


</body>


<script src="https://www.google.com/recaptcha/api.js?render=6LcHocoUAAAAAITkd3OuSwY6JiZqqbQjEKxkNCek"></script>
<script src="{{ asset('/js/app.js') }}"></script>

<script>

</script>
</html>
