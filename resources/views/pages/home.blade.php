@extends('base')

@section('title', 'СТО, Сварочные работы, Строительные работы, Ремонт авто')
@section('page-class', 'home')
@section('description', $settings['about_us'] ?? '')


@section('content')

        @if(isset($settings['slider']))
        <section class="slider">
            <div class="glide">
                <div data-glide-el="track" class="glide__track">
                    @if(count(unserialize($settings['slider'])) > 0)
                        <ul class="glide__slides">
                            @foreach(unserialize($settings['slider']) as $slide)
                                <li class="glide__slide"><img src="/images/{{$slide}}" alt="{{$settings['title'] ?? 'Картинка слайдера'}}"/></li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
            <div class="about">
                <h1>Sun Grad <br/> {{$settings['title'] ?? ''}}</h1>
                <div class="about-text">
                    {{$settings['about_us'] ?? ''}}
                </div>
                <div class="buttons">
                    <button class="btn btn-success __order" data-custom-open="modal-1">Связатся с нами</button>
                </div>
            </div>
        </section>
        @endif
        <section class="services">
            <span class="h1">Наши услуги</span>
            @foreach ($services as $service)
            <div class="service">

                <h3><a href="/{{$service->slug}}">{{$service->title}}</a></h3>

                <div class="service-in">
                    @if(isset($service->images[0]))
                        <img src="/images/cut-{{$service->images[0]->file_name}}" alt="{{$service->name}}">
                    @endif
                    <div class="explain">
                        <p>
                            {{Str::limit(strip_tags(preg_replace('#<[^>]+>#', ' ',$service->description)), 300)}}
                        </p>
                        <div class="service-btn">
                            <button class="btn btn-success __order" data-custom-open="modal-2">Заказать</button>
                            <a href="/{{$service->slug}}" class="btn">Больше информации</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </section>

@endsection
