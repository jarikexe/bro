@extends('base')

@section('title', $service->title)
@section('page-class', 'home')
@section('description', $service->seo_description)


@section('content')
    <h1>{{$service->title}}</h1>

    <div class="service-page">
        <div class="glide2">
            <div data-glide-el="track" class="glide__track">
                @if(count($service->images) > 0)
                <div id="image" class="glide__slides">
                    @foreach($service->images as $image)
                        <img class="sludes" data-zoomable src="/images/small-{{$image->file_name}}" data-zoom-src="/images/{{$image->file_name}}" alt="{{$service->name}}">
                    @endforeach
                </div>
                @endif

            </div>
            <div class="glide__arrows" data-glide-el="controls">
                <span class="glide__arrow glide__arrow--left" data-glide-dir="<">
                    <span class="icon-chevron-left"></span>
                </span>
                <span class="glide__arrow glide__arrow--right" data-glide-dir=">">
                    <span class="icon-chevron-right1"></span>
                </span>
            </div>
        </div>

        <div class="explain-in">
            {!! $service->description !!}
        </div>
    </div>
    <div class="service-in-btn">
        <button class="btn btn-success __order" name="contact" data-custom-open="modal-2">Заказать</button>
        <button class="btn" data-custom-open="modal-3">Обратный звонок</button>
    </div>

@endsection
