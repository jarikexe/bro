<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ServiceController@index');


Route::get('/admin', 'AdminController@index')
    ->name('admin')
    ->middleware('auth');
Route::post('/services', 'ServiceController@store')
    ->name('service.store')
    ->middleware('auth');
Route::get('/services/{service}/edit', 'ServiceController@edit')
    ->name('service.edit')
    ->middleware('auth');
Route::put('/services/{service}', 'ServiceController@update')
    ->name('service.edit')
    ->middleware('auth');
Route::get('/services/{service}/delete', 'ServiceController@destroy')
    ->name('service.edit')
    ->middleware('auth');
Route::get('/service/create', 'ServiceController@create')
    ->name('service.create')
    ->middleware('auth');

Route::get('/settings/edit', 'SettingsController@edit')
    ->name('settings.edit')
    ->middleware('auth');
Route::put('/settings/update', 'SettingsController@update')
    ->name('settings.update')
    ->middleware('auth');
Route::get('/settings/slider/edit', 'SettingsController@sliderShow')
    ->name('settings.slider.update')
    ->middleware('auth');
Route::post('/settings/slider/save', 'SettingsController@slederUpdate')
    ->name('settings.slider.save')
    ->middleware('auth');
Route::post('/settings/slider/order', 'SettingsController@slederOrder')
    ->name('settings.slider.order')
    ->middleware('auth');
Route::get('/settings/slider/{name}/remove', 'SettingsController@slideRemoce')
    ->name('settings.slider.remove')
    ->middleware('auth');
Route::get('/settings/change-password', 'SettingsController@passwordChange')
    ->name('settings.password.change')
    ->middleware('auth');
Route::post('/settings/change-password', 'SettingsController@passwordStore')
    ->name('settings.password.change')
    ->middleware('auth');

Route::get('/orders/{page?}', 'OrderController@index')
    ->name('orders')
    ->middleware('auth');
Route::post('/orders', 'OrderController@store')->name('order.create');

Route::post('/upload', 'ImagesController@save')
    ->name('image.store')
    ->middleware('auth');

Route::get('/services/{service}/pictures', 'ImagesController@index')->name('image.index');

Route::post('/images/order', 'ImagesController@updateOrder')->name('image.order');
Route::get('/image/{name}/delete', 'ImagesController@delete')
    ->name('image.delete')
    ->middleware('auth');

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::get('/{slug}', 'ServiceController@show');



